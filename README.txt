Tweetbacks for Drupal
=====================


Tweetbacks is a module allowing you to show Twitter posts which refer to pages
on your Drupal website in a block on those pages. It also provides a second
block showing the pages with the most referring tweets.

Using the Module
----------------

1. Install as usual (usually in sites/all/modules)
2. Open http://your-site/admin/build/block
3. Turn on and configure the Tweetbacks block.
4. (Optional) Turn on and configure the Tweetback Statistics block.

Frequently Asked Questions
--------------------------

What are Tweetbacks?

    Tweetbacks are Twitter messages that refer to pages on your website.
Whenever a Twitter user posts a message with a URL, it is a tweetback for that
particular URL.

Why would I want to use this module?

    Do you like to see when people are talking about and sharing your site's
content? Tweetbacks allows you to see when people are talking about your site
on Twitter. By displaying a list of tweets (Twitter posts) that reference the
pages on your site, you and your readers can get an idea of how popular or
important the content is, on your site.

Who developed Tweetbacks?

    The original Tweetbacks service was developed by Dan Zarrella
<http://danzarrella.com/>. Chris Charabaruk <http://coldacid.net> wrote the
Drupal module for Tweetbacks, making use of Dan's service to provide not just
Tweetbacks for each page but also a list of the most popular pages based on
Tweetbacks.
